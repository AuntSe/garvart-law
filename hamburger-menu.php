	<?php
		session_start();
		if (!isset($_SESSION['lang'])) {
				$_SESSION['lang'] = "lang-ru"; 
			}
	?>
<ul id="hidden-menu">
	<li id="close-hidden-menu"><img src="img/close-menu.png" id="close"></li>
	<li class="hidden-menu-item"><a href="/maintenance<?php echo "?lang=".$_SESSION['lang'];?>"><?php include ($_SESSION['lang']."/maintenance_header.txt");?></a></li>
	<li class="hidden-menu-item"><a href="/agency<?php echo "?lang=".$_SESSION['lang'];?>"><?php include ($_SESSION['lang']."/main_menu_2.txt");?></a></li>
	<li class="hidden-menu-item"><a href="/documents<?php echo "?lang=".$_SESSION['lang'];?>"><?php include ($_SESSION['lang']."/documents_header.txt");?></a></li>
	<li class="hidden-menu-item"><a href="/consultations<?php echo "?lang=".$_SESSION['lang'];?>"><?php include ($_SESSION['lang']."/main_menu_4.txt");?></a></li>
	<li class="hidden-menu-item"><a href="/our-contacts<?php echo "?lang=".$_SESSION['lang'];?>" id="contacts-page"><?php include ($_SESSION['lang']."/arrow_main.txt");?></a></li>
	<li class="hidden-menu-item"><a href="/index.php<?php echo "?lang=".$_SESSION['lang'];?>"><?php include ($_SESSION['lang']."/arrow_contacts.txt");?></a></li>
</ul>