<!DOCTYPE html>
<html>
	<head>
		<?php
			session_start();
			if (!isset($_SESSION['lang'])) {
				$_SESSION['lang'] = "lang-ru"; 
			}
			if (isset($_GET["lang"])) {
				$_SESSION['lang']=$_GET["lang"];
			}
			else {
				header("Location: /agency.php?lang=".$_SESSION['lang']);
			}
		?>
		<title><?php include ($_SESSION['lang']."/agency_title.txt");?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="description" content="<?php include ($_SESSION['lang']."/agency_description.txt");?>">
		<meta name="Keywords" content="<?php include ($_SESSION['lang']."/agency_keywords.txt");?>">
		<link rel="stylesheet" type="text/css" href="css/main.css">
		<link rel="stylesheet" type="text/css" href="css/others.css">
		<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
		<script src="js/main.js" async></script>
	<head>
	<body>
		<div id="wrapper">
			<img src="img/hamburger-yellow.png" id="hamburger" class="hamburger-yellow">
			<?php
				require "hamburger-menu.php";
			?>
			<?php
				require "languages.php";
			?>
			<div class="container" id="container-agency">
				<p id="header">
					<?php include ($_SESSION['lang']."/main_menu_2.txt");?>
				</p>
				<div id="block-logo-others">
					<img src="img/logo.png" class="logo" id="logo-others">
					<div id="text">
						<?php include ($_SESSION['lang']."/agency.txt");?>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>