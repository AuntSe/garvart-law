window.onload = function() {
	var outWrapper = document.getElementById("wrapper");
	var wrapper = document.getElementsByClassName("container");
	var body = document.getElementsByTagName("body");
	var docWidth, docHeight, wrapperWidth, wrapperHeight;
	
	var langEn = document.getElementById("lang-en");
	var langRu = document.getElementById("lang-ru");
	
	var languages = document.getElementById("languages");
	var menu = document.getElementById("menu");
	var hiddenMenu = document.getElementById("hidden-menu");
	var name = document.getElementById("name");
	var arrowBlock = document.getElementsByClassName("arrow-block");
	var info = document.getElementById("information");
	var slogan = document.getElementById("slogan");
	var contacts = document.getElementById("block-contacts");
	
	var menuItem = document.getElementsByClassName("menu-item");
	var logoBlock = document.getElementById("block-logo");
	
	var arrowMain = document.getElementById("arrow-block-index");
	var arrowContacts = document.getElementById("arrow-block-contacts");
	var containerMain = document.getElementById("container-main");
	var containerContacts = document.getElementById("container-contacts");
	
	var arrowText = document.getElementsByClassName("arrow-text");
	var arrowLeft = document.getElementById("arrow-left");
	var arrowRight = document.getElementById("arrow-right");
	var hamburger = document.getElementById("hamburger");
	var closeSign = document.getElementById("close");
	
	var textLogoBlock = document.getElementById("block-logo-others");
	var textHeader = document.getElementById("header");
	
	var contactsLink = document.getElementById("contacts-page");
	
	var hamburgerYellow = document.getElementsByClassName("hamburger-yellow");
	
	var containerMaintenance = document.getElementById("container-maintenance");
	var containerAgency = document.getElementById("container-agency");
	var containerDocuments = document.getElementById("container-documents");
	var containerConsultations = document.getElementById("container-consultations");
	
// Изменение положения и размеров элементов при изменении размеров окна
	
	window.addEventListener("resize", resizing);
// Изменение положения и размеров элементов в зависимости от размеров окна
	resizing();
	background();
//Элементы, которые есть на всех страницах (скрытое меню)
	hamburger.addEventListener("mouseenter", hamburgerHover);
	hamburger.addEventListener("mouseleave", hamburgerUnHover);
	closeSign.addEventListener("mouseenter", closeHover);
	closeSign.addEventListener("mouseleave", closeUnHover);
	hamburger.onclick = function() {
		hiddenMenu.style.display = ("block");
		hiddenUndiddenMenu();
	}
	closeSign.onclick = function() {
		hiddenMenu.style.display = ("none");
		hiddenUndiddenMenu();
	}
	
	for(i=0; i<wrapper.length; i++){
		wrapper[i].onclick = function() {
			if (window.getComputedStyle(hiddenMenu).display == "block"){
				hiddenMenu.style.display = ("none");
			}
			hiddenUndiddenMenu();
		}
	}

//Элементы, которые есть только на главной/странице контактов (стрелки)	

	if(containerMain) {
		hoverUnhover(arrowBlock, arrowHover, arrowUnHover);
	}

// Функция, предназначенная для отображения или скрывания скрытого меню
	
	function hiddenUndiddenMenu() {
		if (hiddenMenu.style.display == "block") {
			hamburger.style.display = ("none");
			if (containerMain) {
				menu.style.display = ("none");
				logoBlock.style.marginLeft = ("47%");
				info.style.marginLeft = ("31%");
				info.style.width = ("25%");
				for (i=0; i<arrowBlock.length; i++) {
					arrowBlock[i].style.display = ("none");
				}
				slogan.style.marginLeft = ("5%");
				contacts.style.marginLeft = ("45%");
			}
			else {
				textLogoBlock.style.marginLeft = ("32%");
				textHeader.style.marginRight = ("8%");
			}
		}
		else {
			hamburger.style.display = ("");
			if (containerMain) {
				menu.style.display = ("");
				logoBlock.style.marginLeft = ("1%");
				info.style.marginLeft = ("1%");
				info.style.width = ("40%");
				for (i=0; i<arrowBlock.length; i++) {
					arrowBlock[i].style.display = ("");
				}
				slogan.style.marginLeft = ("20%");
				contacts.style.marginLeft = ("30%");
			}
			else {
				textLogoBlock.style.marginLeft = ("auto");
				textLogoBlock.style.marginRight = ("auto");
				textHeader.style.marginRight = ("20%");
			}
		}
	}

// Функции, описывающие поведение различных элементов при наведении на них мыши
// и ее отведении от них
	
	function arrowHover() {
		arrowText[0].style.color = ("#c4b159");
		arrowText[1].style.color = ("#c4b159");
		arrowLeft.src = ("../img/arrow-left-hovered.png");
		arrowRight.src = ("../img/arrow-right-hovered.png");
	}
	function arrowUnHover() {
		arrowText[0].style.color = ("#fedb3c");
		arrowText[1].style.color = ("#fedb3c");
		arrowLeft.src = ("../img/arrow-left.png");
		arrowRight.src = ("../img/arrow-right.png");
	}
	
	function hamburgerHover() {
		hamburger.src = ("../img/hamburger-hovered.png");
	}
	function hamburgerUnHover() {
		if (hamburgerYellow[0]) {
			hamburger.src = ("../img/hamburger-yellow.png");
		}
		else {
			hamburger.src = ("../img/hamburger.png");
		}
	}
	
	function closeHover() {
		closeSign.src = ("../img/close-menu-hovered.png");
	}
	function closeUnHover() {
		closeSign.src = ("../img/close-menu.png");
	}
	
	function hoverUnhover(elem, funcHover, funcUnhover) {
		for (i=0; i<elem.length; i++){
			elem[i].addEventListener("mouseenter", funcHover);
			elem[i].addEventListener("mouseleave", funcUnhover);
		}
	}

// Фкнуция, меняющая расположение и размеры элементов страниц в зависимости от ращмеров окна

	function resizing() {
		docWidth = 0
		docHeight = 0;
		body[0].style.width = (0);
		body[0].style.height = (0);
		outWrapper.style.width = (0);
		outWrapper.style.height = (0);
		for (i=0;i<wrapper.length;i++) {
			wrapper[i].style.width = (0);
			wrapper[i].style.height = (0);
		}
		
		docWidth = document.documentElement.clientWidth;
		docHeight = document.documentElement.clientHeight;
		body[0].style.width = (docWidth + "px");
		body[0].style.height = (docHeight + "px");
		outWrapper.style.width = (docWidth + "px");
		outWrapper.style.height = (docHeight + "px");
		for (i=0;i<wrapper.length;i++) {
			wrapper[i].style.width = (docWidth + "px");
			wrapper[i].style.height = (docHeight + "px");
		}
		wrapperWidth = outWrapper.offsetWidth;
		wrapperHeight = outWrapper.offsetHeight;
		var sidesRelation=wrapperWidth/wrapperHeight;
		
		if (wrapperHeight<=wrapperWidth) { //горизонтальный экран
			
			// Элементы, которые есть на всех страницах
			languages.style.fontSize = (wrapperWidth*1.6/100 + "px");
			hiddenMenu.style.fontSize = (wrapperWidth*2/100 + "px");
			hiddenMenu.style.lineHeight = (wrapperWidth*4/100 + "px");	
			hiddenMenu.style.width = ("30%");
			hamburger.style.width = ("5%");
			
			if (containerMain) { // Элементы, которые есть только на главной/странице контактов
				contacts.style.fontSize = (wrapperWidth*2/100 + "px");
				contacts.style.lineHeight = (wrapperWidth*3/100 + "px");
				contacts.style.width = ("40%");
				for (i=0;i<arrowBlock.length;i++) {
					arrowBlock[i].style.fontSize = (wrapperWidth*2.1/100 + "px");
					arrowBlock[i].style.lineHeight = (wrapperWidth*6.6/100 + "px");
					arrowBlock[i].style.width = ("18%");
				}
				arrowLeft.style.width = ("23%");
				arrowRight.style.width = ("23%");
				menu.style.fontSize = (wrapperWidth*1.6/100 + "px");
				menu.style.lineHeight = (wrapperWidth*2.1/100 + "px");
				menu.style.display = ("");
				name.style.fontSize = (wrapperWidth*2.9/100 + "px"); 
				info.style.fontSize = (wrapperWidth*0.9/100 + "px");
				info.style.lineHeight = (wrapperWidth*1.6/100 + "px");
				slogan.style.fontSize = (wrapperWidth*2.4/100 + "px");
				for(var i=0; i<menuItem.length;i++) {
					menuItem[i].style.height = (wrapperWidth*6/100 + "px");
				}
				name.style.marginTop = (wrapperWidth*3.7/100 + "px"); 
				logoBlock.style.width = ("37%");
				logoBlock.style.height = (wrapperWidth*15/100 + "px");
					if(sidesRelation<1.62) { // Экран ближе к прямоугольному
					logoBlock.style.marginTop = (wrapperHeight*15.7/100 + "px");
					logoBlock.style.marginBottom = (wrapperHeight*31.6/100 + "px");
					contacts.style.marginTop = (wrapperWidth*19.3/100 + "px");
				}
				else { //высота намного меньше ширины
					logoBlock.style.marginTop = (wrapperHeight*9.5/100 + "px");
					logoBlock.style.marginBottom = (wrapperHeight*15/100 + "px");
					contacts.style.marginTop = (wrapperWidth*10.3/100 + "px");
				}	
			}
			else { //Элементы, которые есть только на страницах разделов
				textLogoBlock.style.fontSize = (wrapperWidth*1.5/100 + "px");
				textHeader.style.fontSize = (wrapperWidth*2.2/100 + "px");
				if(sidesRelation<1.62) {
					textLogoBlock.style.marginTop = (wrapperHeight*25/100 + "px");
					textHeader.style.marginTop = (wrapperHeight*17/100 + "px");
				}
				else {
					textLogoBlock.style.marginTop = (wrapperHeight*15/100 + "px");
					textHeader.style.marginTop = (wrapperHeight*3/100 + "px");
				}
			}

		}
		
		
		else { //вертикальный экран
			
			// Элементы, которые есть на всех страницах
			languages.style.fontSize = (wrapperHeight*1.6/100 + "px");
			hiddenMenu.style.fontSize = (wrapperWidth*3/100 + "px");
			hiddenMenu.style.lineHeight = (wrapperWidth*6/100 + "px");
			hiddenMenu.style.width = ("60%");
			hamburger.style.width = ("10%");
			
			if (containerMain) { // Элементы, которые есть только на главной/странице контактов
				contacts.style.fontSize = (wrapperHeight*2/100 + "px");
				contacts.style.lineHeight = (wrapperHeight*3.5/100 + "px");
				contacts.style.width = ("50%");
				for (i=0;i<arrowBlock.length;i++) {
					arrowBlock[i].style.fontSize = (wrapperHeight*1.8/100 + "px");
					arrowBlock[i].style.lineHeight = (wrapperHeight*3.6/100 + "px");
				}
				arrowLeft.style.width = ("15%");
				arrowRight.style.width = ("15%");
				menu.style.fontSize = (wrapperHeight*1.6/100 + "px");
				menu.style.lineHeight = (wrapperHeight*2.1/100 + "px");
				menu.style.display = ("none");
				name.style.fontSize = (wrapperHeight*2.9/100 + "px"); 
				info.style.fontSize = (wrapperHeight*0.9/100 + "px");
				info.style.lineHeight = (wrapperHeight*1.6/100 + "px");
				slogan.style.fontSize = (wrapperHeight*2.4/100 + "px");
				for(var i=0; i<menuItem.length;i++) {
					menuItem[i].style.height = (wrapperHeight*6/100 + "px");
				}
				name.style.marginTop = (wrapperHeight*3.8/100 + "px"); 
				logoBlock.style.marginTop = (wrapperHeight*35/100 + "px");
				logoBlock.style.marginBottom = (wrapperHeight*30/100 + "px");
				contacts.style.marginTop = (wrapperHeight*30/100 + "px")
				if(sidesRelation<0.65) {
					for (i=0;i<arrowBlock.length;i++) {
						arrowBlock[i].style.width = ("27%");
					}
					logoBlock.style.width = ("60%");
					logoBlock.style.height = (wrapperWidth*24/100 + "px");
				}
				else {
					for (i=0;i<arrowBlock.length;i++) {
						arrowBlock[i].style.width = ("22%");
					}
					logoBlock.style.width = ("50%");
					logoBlock.style.height = (wrapperWidth*20/100 + "px");
				}
			}	
			else { //Элементы, которые есть только на страницах разделов
				textLogoBlock.style.fontSize = (wrapperHeight*1.5/100 + "px");
				textHeader.style.fontSize = (wrapperHeight*2.2/100 + "px");
				textLogoBlock.style.marginTop = (wrapperHeight*25/100 + "px");
				textHeader.style.marginTop = (wrapperHeight*20/100 + "px");
			}
		}
	}
	
	function background() {
		if (containerMain) {
			containerMain.style.backgroundImage = ("url(../img/main-background.jpg)");
			containerContacts.style.backgroundImage = ("url(../img/contacts-background.jpg)");
		}
		else if(containerMaintenance) {
			containerMaintenance.style.backgroundImage = ("url(../img/maintenance-background.jpg)");
		}
		else if(containerAgency) {
			containerAgency.style.backgroundImage = ("url(../img/agency-background.jpg)");
		}
		else if(containerDocuments) {
			containerDocuments.style.backgroundImage = ("url(../img/documents-background.JPG)");
		}
		else if(containerConsultations) {
			containerConsultations.style.backgroundImage = ("url(../img/consultations-background.jpg)");
		}
	}
}