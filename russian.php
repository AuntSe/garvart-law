<?php 
	session_start();
	$_SESSION['lang'] = "lang-ru";
	$_SERVER['HTTP_REFERER']=rgp($_SERVER['HTTP_REFERER']);
	if($_SESSION['contacts']=="our-contacts") {
		$_SERVER['HTTP_REFERER']=$_SERVER['HTTP_REFERER']."?contacts=".$_SESSION['contacts'];
		$_SERVER['HTTP_REFERER']=$_SERVER['HTTP_REFERER']."&lang=".$_SESSION['lang'];
	}
	else {
		$_SERVER['HTTP_REFERER']=$_SERVER['HTTP_REFERER']."?lang=".$_SESSION['lang'];
	}
	$_SESSION['contacts']=" ";
	function goback()
	{
		header("Location: {$_SERVER['HTTP_REFERER']}");
		exit;
	}
	function rgp($url) 
	{ // remove GET-parameters from URL
		return preg_replace('/^([^?]+)(\?.*?)?(#.*)?$/', '$1$3', $url);
	}
  
	goback();
?>