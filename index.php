<!DOCTYPE html>
<html>
	<head>
		<?php
			session_start();
			if (!isset($_SESSION['lang'])) {
				$_SESSION['lang'] = "lang-ru";
			}
			if (isset($_GET["lang"])) {
				$_SESSION['lang']=$_GET["lang"];
			}
			else {
				header("Location: index.php?lang=".$_SESSION['lang']);
			}
			if($_GET["contacts"]=="our-contacts") {
				$_SESSION['contacts']="our-contacts";
			}
			else {
				$_SESSION['contacts']=" ";
			}
		?>
		<title><?php include ($_SESSION['lang']."/main_title.txt");?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="description" content="<?php include ($_SESSION['lang']."/main_description.txt");?>">
		<meta name="Keywords" content="<?php include ($_SESSION['lang']."/main_keywords.txt");?>">
		<link rel="stylesheet" type="text/css" href="css/main.css">
		<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
		<script src="js/main.js"></script>
	<head>
	<body>
		<div id="wrapper">
			<img src="img/hamburger.png" id="hamburger">
			<?php
				require "hamburger-menu.php";
			?>
			<?php
				require "languages.php";
			?>
			<div class="container" id="container-main"
				<?php 
					if($_GET["contacts"]=="our-contacts") {
						echo ' style="display: none"';
					}
				?>
			>
				<ul id="menu">
					<li class="menu-item"><div><a href="/maintenance<?php echo "?lang=".$_SESSION['lang'];?>"><?php include ($_SESSION['lang']."/main_menu_1.txt");?></a></div></li>
					<li class="menu-item"><div><a href="/agency<?php echo "?lang=".$_SESSION['lang'];?>"><?php include ($_SESSION['lang']."/main_menu_2.txt");?></a></div></li>
					<li class="menu-item"><div><a href="/documents<?php echo "?lang=".$_SESSION['lang'];?>"><?php include ($_SESSION['lang']."/main_menu_3.txt");?></a></div></li>
					<li class="menu-item"><div><a href="/consultations<?php echo "?lang=".$_SESSION['lang'];?>"><?php include ($_SESSION['lang']."/main_menu_4.txt");?></a></div></li>
				</ul>
				<div id="block-logo">
					<img src="img/logo.png" class="logo" id="logo-index">
					<p id="name"><?php include ($_SESSION['lang']."/name.txt");?></p>
				</div>
				<a href="index.php?contacts=our-contacts&<?php echo "lang=".$_SESSION['lang'];?>">
					<div class="arrow-block" id="arrow-block-index">
						<img src="img/arrow-right.png" class="arrow" id="arrow-right">
						<p class="arrow-text"><?php include ($_SESSION['lang']."/arrow_main.txt");?></p>
					</div>
				</a>
				<p class="text-block" id="information">
					<?php include ($_SESSION['lang']."/information.txt");?>
				</p>
				<p class="text-block" id="slogan">
					<?php include ($_SESSION['lang']."/slogan.txt");?>
				</p>
			</div>
			<div class="container" id="container-contacts"
				<?php 
					if($_GET["contacts"]=="our-contacts") {
						echo ' style="display: block"';
					}
				?>
			>
			<div id="block-contacts">
				<div id="block-logo-contacts">
					<img src="img/logo.png" class="logo" id="logo-contacts">
				</div>
				<div id="background-logo-top">
				</div>
				<div id="background-logo-left">
				</div>
				<?php include ($_SESSION['lang']."/contacts.txt");?>
			</div>
			<a href="index.php<?php echo "?lang=".$_SESSION['lang'];?>">
				<div class="arrow-block" id="arrow-block-contacts">
					<img src="img/arrow-left.png" class="arrow" id="arrow-left">
					<p class="arrow-text"><?php include ($_SESSION['lang']."/arrow_contacts.txt");?></p>
				</div>
			</a>
		</div>
		</div>
	<body>
</html>